﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MessageBox = System.Windows.Forms.MessageBox;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using SectionView.Extends;
using Autodesk.Revit.Attributes;

namespace SectionView
{
    [Transaction(TransactionMode.Manual)]
    [Journaling(JournalingMode.NoCommandData)]
    [Regeneration(RegenerationOption.Manual)]
    public class SectionViewJudgeElementOcclusionCmd : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Options Options = new Options()
            {
                ComputeReferences = true,
                DetailLevel = ViewDetailLevel.Fine
            };

            UIDocument UiDoc = commandData.Application.ActiveUIDocument;
            Document Doc = commandData.Application.ActiveUIDocument.Document;

            Element checkElement = null;
            List<Element> relevanceElements = new List<Element>();
            do
            {
                try
                {
                    MessageBox.Show("请选择需要检查是否被遮挡构件");
                    checkElement = null;
                    relevanceElements = new List<Element>();
                    Reference checkReference = UiDoc.Selection.PickObject(ObjectType.Element,
                        "请选择需要检查是否被遮挡构件");
                    if (checkReference != null)
                    {
                        checkElement = Doc.GetElement(checkReference);
                    }

                    MessageBox.Show("请选择与其相关构件");
                    IList<Reference> relevanceReferences = UiDoc.Selection.PickObjects(ObjectType.Element,
                        "请选择与其相关构件");
                    if (!relevanceReferences.IsNullOrEmpty())
                    {
                        foreach (Reference reference in relevanceReferences)
                        {
                            Element element = Doc.GetElement(reference);
                            if (element != null && element.Id != checkElement.Id)
                            {
                                relevanceElements.Add(element);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    break;
                }
            } while (checkElement == null || relevanceElements.IsNullOrEmpty());
            if (checkElement != null && !relevanceElements.IsNullOrEmpty())
            {

                UiDoc = commandData.Application.ActiveUIDocument;
                Doc = commandData.Application.ActiveUIDocument.Document;

                using (TransactionGroup transGroup = new TransactionGroup(Doc, "事务组"))
                {
                    try
                    {
                        if (transGroup.Start() == TransactionStatus.Started)
                        {
                            List<Element> sourceElements = new List<Element>() { checkElement };
                            sourceElements.AddRange(relevanceElements);

                            using (Transaction trans = new Transaction(Doc, "取消连接和剪切"))
                            {
                                try
                                {
                                    trans.Start();
                                    foreach (Element element in sourceElements)
                                    {
                                        ICollection<ElementId> joinElements = JoinGeometryUtils.GetJoinedElements(Doc, element);
                                        if (!joinElements.IsNullOrEmpty())
                                        {
                                            foreach (ElementId id in joinElements)
                                            {
                                                Element joinElement = Doc.GetElement(id);
                                                JoinGeometryUtils.UnjoinGeometry(Doc, joinElement, element);
                                            }
                                        }
                                        ICollection<ElementId> cutElements = SolidSolidCutUtils.GetCuttingSolids(element);
                                        if (!cutElements.IsNullOrEmpty())
                                        {
                                            foreach (ElementId id in cutElements)
                                            {
                                                Element cutElement = Doc.GetElement(id);
                                                SolidSolidCutUtils.RemoveCutBetweenSolids(Doc, cutElement, element);
                                            }
                                        }
                                    }

                                    trans.Commit();
                                }
                                catch (Exception)
                                {
                                    trans.RollBack();
                                }
                            }
                            View currentView = Doc.ActiveView;
                            Dictionary<ElementId, KeyValuePair<double, Solid>> sectionSolids = GetViewSectionLinesEx(
                                currentView, Doc, Options, sourceElements);
                            if (!sectionSolids.IsNullOrEmpty())
                            {
                                //根据剖面距离进行排序
                                sectionSolids = sectionSolids.OrderBy(a => a.Value.Key).ToDictionary(a => a.Key, b => b.Value);
                                List<ElementId> elementIdStrings = sectionSolids.Select(a => a.Key).ToList();
                                int checkIndex = elementIdStrings.IndexOf(checkElement.Id);
                                if (checkIndex >= 0)
                                {
                                    foreach (Element relevanceElement in relevanceElements)
                                    {
                                        int basisIndex = elementIdStrings.IndexOf(relevanceElement.Id);
                                        if (basisIndex >= 0)
                                        {
                                            if (checkIndex > basisIndex)
                                            {
                                                var checkKV = sectionSolids[sectionSolids.Keys.ToList()[checkIndex]];
                                                var basisKV = sectionSolids[sectionSolids.Keys.ToList()[basisIndex]];
                                                if (!checkKV.Key.AreEqual(basisKV.Key))
                                                {
                                                    IsCompleteOverlap(checkKV.Value, basisKV.Value);
                                                }
                                                else
                                                {
                                                    //未被遮挡
                                                    MessageBox.Show("未被遮挡");
                                                }
                                            }
                                            else
                                            {
                                                //未被遮挡
                                                MessageBox.Show("未被遮挡");
                                            }

                                            //放置实体
                                            //Doc.TransactionBlock(() =>
                                            //{
                                            //    foreach (KeyValuePair<double, Solid> linesKV in sectionSolids.Values)
                                            //    {
                                            //        DirectShape shape = DirectShape.CreateElement(RevitVariable.Doc,
                                            //            new ElementId(BuiltInCategory.OST_StructuralFoundation));
                                            //        shape.AppendShape(new List<GeometryObject>() { linesKV.Value });
                                            //    }
                                            //}, "判断相交面");
                                        }
                                    }
                                }
                                else
                                {
                                    //被全部遮挡
                                    MessageBox.Show("检查构件在剖面框外");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("发生错误：{0}", ex.Message));
                    }
                    finally
                    {
                        transGroup.RollBack();
                    }
                }
            }
            return Result.Cancelled;
        }

        public void IsCompleteOverlap(Solid checkSolid, Solid basisSolid)
        {
            //获取实体的交集
            Solid solidBool = null;
            try
            {
                try
                {

                    solidBool = BooleanOperationsUtils.ExecuteBooleanOperation(checkSolid, basisSolid, BooleanOperationsType.Intersect);
                }
                catch (Exception)
                {
                    solidBool = BooleanOperationsUtils.ExecuteBooleanOperation(basisSolid, checkSolid, BooleanOperationsType.Intersect);
                }
                if (solidBool != null && solidBool.Volume > 0)
                {
                    if (solidBool.Volume.AreEqual(checkSolid.Volume))
                    {
                        MessageBox.Show("被全部遮挡");
                    }
                    else
                    {
                        MessageBox.Show("存在部分遮挡");
                    }
                }
                else
                {
                    MessageBox.Show("未被遮挡");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("存在部分遮挡");
                //if (checkSolid.Volume.AreEqual(checkSolid.Volume))
                //{
                //    MessageBox.Show("被全部遮挡");
                //}
                //else
                //{
                //}
            }

        }
        public Dictionary<ElementId, KeyValuePair<double, Solid>> GetViewSectionLinesEx(View view, Document doc, Options option, List<Element> sourceElements)
        {
            //if (view.ViewType != ViewType.Section) return null;

            Dictionary<ElementId, KeyValuePair<double, Solid>> sectionFaces =
                new Dictionary<ElementId, KeyValuePair<double, Solid>>();
            Plane plane = Plane.CreateByOriginAndBasis(view.Origin, view.RightDirection, view.UpDirection);
            XYZ viewVector = view.ViewDirection.Normalize();

            List<ElementId> elementIds = new List<ElementId>();
            foreach (Element element in sourceElements)
            {
                IList<Solid> solids = element.GetSolids(option);
                Solid solid = solids?.OrderByDescending(a => a.Volume).FirstOrDefault();
                if (solid != null)
                {
                    //Face upFace = null;
                    //Face downFace = null;
                    ////判断是否为竖直上下面为圆形构件
                    //foreach (Face face in solid.GetFaces())
                    //{
                    //    var faceNormal = face.GetUVCenterPointNormal();
                    //    if (faceNormal.AreEqual(new XYZ(0, 0, 1)))
                    //    {
                    //        foreach (Curve curve in face.EdgeLoops.get_Item(0))
                    //        {
                    //            if (curve is Arc arc)
                    //            {
                    //                upFace = face;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    if (faceNormal.AreEqual(new XYZ(0, 0, -1)))
                    //    {
                    //        foreach (Curve curve in face.EdgeLoops.get_Item(0))
                    //        {
                    //            if (curve is Arc arc)
                    //            {
                    //                downFace = face;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //}
                    //if (upFace != null && downFace != null)
                    //{
                    //    XYZ upCenterPt = FaceEx.GetUVCenterPoint(upFace);
                    //    XYZ downCenterPt = FaceEx.GetUVCenterPoint(downFace);
                    //    if (upCenterPt.AreEqual(downCenterPt))
                    //    {
                    //        //竖直

                    //    }
                    //    else
                    //    {

                    //    }
                    //}


                    double minDis = double.MaxValue;
                    List<Line> lines = new List<Line>();
                    List<Face> faces = solid.GetFaces();

                    List<Solid> solidsTmp = new List<Solid>();
                    bool judge = false;
                    double length = 0;
                    foreach (Face face in faces)
                    {
                        if (face is CylindricalFace cylindricalFace)
                        {
                            CurveLoop curveLoop = face.GetEdgesAsCurveLoops().FirstOrDefault();
                            if (curveLoop != null)
                            {
                                foreach (Curve curve in curveLoop)
                                {
                                    if (curve is Arc arc)
                                    {
                                        List<Line> newLines = plane.PointProjectPlane(arc);
                                        if (newLines.Last().IsIntersect(newLines.First()))
                                        {
                                            try
                                            {
                                                Solid solidTmp = GeometryCreationUtilities.CreateExtrusionGeometry(
                                                    new List<CurveLoop>() { CurveLoop.Create(new List<Curve>(newLines)) },
                                                    view.ViewDirection, 2);
                                                solidsTmp.Add(solidTmp);
                                                continue;
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        foreach (Line newLine in newLines)
                                        {
                                            double startDis = arc.StartPoint().DistanceTo(newLine.StartPoint());
                                            double endDis = arc.EndPoint().DistanceTo(newLine.EndPoint());
                                            if (minDis > startDis)
                                            {
                                                minDis = startDis;
                                            }
                                            if (minDis > endDis)
                                            {
                                                minDis = endDis;
                                            }
                                            if (OptimizeLines(newLine, ref lines))
                                            {
                                                continue;
                                            }
                                            lines.Add(newLine);
                                        }
                                    }
                                    else if (curve is Line line)
                                    {
                                        if (length == 0)
                                        {
                                            length = line.Length;
                                        }
                                        else
                                        {
                                            if (length.AreEqual(line.Length))
                                            {
                                                judge = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            List<Line> lis = new List<Line>();
                            EdgeArray edgeArray = face.EdgeLoops.get_Item(0);
                            foreach (Edge edge in edgeArray)
                            {
                                Curve curve = edge.AsCurve();
                                if (curve is Arc arc)
                                {
                                    List<Line> newLines = plane.PointProjectPlane(arc);
                                    foreach (Line newLine in newLines)
                                    {
                                        double startDis = arc.StartPoint().DistanceTo(newLine.StartPoint());
                                        double endDis = arc.EndPoint().DistanceTo(newLine.EndPoint());
                                        if (minDis > startDis)
                                        {
                                            minDis = startDis;
                                        }
                                        if (minDis > endDis)
                                        {
                                            minDis = endDis;
                                        }
                                        if (OptimizeLines(newLine, ref lines))
                                        {
                                            continue;
                                        }
                                        lines.Add(newLine);
                                    }
                                }
                                else if (curve is Line line)
                                {
                                    Line newLine = plane.PointProjectPlane(line);
                                    if (newLine != null)
                                    {
                                        lis.Add(newLine);
                                        double startDis = line.StartPoint().DistanceTo(newLine.StartPoint());
                                        double endDis = line.EndPoint().DistanceTo(newLine.EndPoint());
                                        if (minDis > startDis)
                                        {
                                            minDis = startDis;
                                        }
                                        if (minDis > endDis)
                                        {
                                            minDis = endDis;
                                        }
                                        if (OptimizeLines(newLine, ref lines))
                                        {
                                            continue;
                                        }
                                        lines.Add(newLine);
                                    }
                                }
                            }
                            string comd121qwqw2 = lis.ToCADCommandFile();

                        }
                    }

                    if (!solidsTmp.IsNullOrEmpty())
                    {
                        try
                        {
                            Solid solidTm = SolidEx.SolidByUnion(solidsTmp);
                            sectionFaces.Add(element.Id, new KeyValuePair<double, Solid>(minDis, solidTm));
                        }
                        catch (Exception)
                        {
                        }
                        continue;
                    }

                    if (judge && lines.Count == 2)
                    {
                        string comd121qwqwewew2 = lines.ToCADCommandFile();
                        double lengthTmp = lines.First().StartPoint().DistanceTo(lines.Last().StartPoint());
                        if (lengthTmp.AreEqual(length))
                        {
                            Line line1 = Line.CreateBound(lines.First().StartPoint(), lines.Last().StartPoint());
                            Line line2 = Line.CreateBound(lines.First().EndPoint(), lines.Last().EndPoint());
                            lines.Add(line1);
                            lines.Add(line2);
                        }
                        else
                        {
                            Line line1 = Line.CreateBound(lines.First().StartPoint(), lines.Last().EndPoint());
                            Line line2 = Line.CreateBound(lines.First().EndPoint(), lines.Last().StartPoint());
                            lines.Add(line1);
                            lines.Add(line2);
                        }
                        string comd = lines.ToCADCommandFile();
                    }
                    string comd1212 = lines.ToCADCommandFile();
                    if (!lines.IsNullOrEmpty())
                    {
                        string comd = lines.ToCADCommandFile();
                        List<Line> newLines = new List<Line>();
                        List<List<Line>> newLiness = new List<List<Line>>();
                        GetCloseLoopLinesEx(null, ref lines, ref newLines, ref newLiness);
                        List<Line> newLinesTmp = newLiness.OrderByDescending(a => a.Sum(c => c.Length)).
                            Where(b => b.Count >= 4).Select(c => c).FirstOrDefault();

                        if (!newLinesTmp.IsNullOrEmpty())
                        {
                            if (newLinesTmp.Last().EndPoint().AreEqual(newLinesTmp.First().StartPoint(), 3))
                            {
                                if (!newLinesTmp.Last().EndPoint().AreEqual(newLinesTmp.First().StartPoint(), 0))
                                {
                                    newLinesTmp[0] = Line.CreateBound(newLinesTmp.Last().EndPoint(),
                                        newLinesTmp.First().EndPoint());
                                }
                            }
                            else
                            {
                                newLinesTmp.Add(Line.CreateBound(newLinesTmp.Last().EndPoint(),
                                    newLinesTmp.First().StartPoint()));
                            }
                            string comd1 = newLinesTmp.ToCADCommandFile();
                            try
                            {
                                List<Curve> curves = new List<Curve>(newLinesTmp);
                                CurveLoop curveLoop = CurveLoop.Create(curves);
                                Solid solidTmp = GeometryCreationUtilities.CreateExtrusionGeometry(
                                     new List<CurveLoop>() { curveLoop }, view.ViewDirection, 2);
                                sectionFaces.Add(element.Id, new KeyValuePair<double, Solid>(minDis, solidTmp));
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }

            return sectionFaces;
        }

        public Dictionary<ElementId, KeyValuePair<double, Solid>> GetViewSectionLinesExOld(View view, Document doc, Options option, List<Element> sourceElements)
        {
            //if (view.ViewType != ViewType.Section) return null;

            Dictionary<ElementId, KeyValuePair<double, Solid>> sectionFaces =
                new Dictionary<ElementId, KeyValuePair<double, Solid>>();
            Plane plane = Plane.CreateByOriginAndBasis(view.Origin, view.RightDirection, view.UpDirection);
            XYZ viewVector = view.ViewDirection.Normalize();

            List<ElementId> elementIds = new List<ElementId>();
            foreach (Element element in sourceElements)
            {
                IList<Solid> solids = element.GetSolids(option);
                Solid solid = solids?.OrderByDescending(a => a.Volume).FirstOrDefault();
                if (solid != null)
                {
                    double minDis = double.MaxValue;
                    List<Line> lines = new List<Line>();
                    List<Face> faces = solid.GetFaces();

                    #region 过时代码
                    //foreach (Face face in faces)
                    //{
                    //    List<XYZ> projectPoints = new List<XYZ>();
                    //    Mesh mesh = face.Triangulate(1);
                    //    foreach (XYZ point in mesh.Vertices)
                    //    {
                    //        XYZ newPoint = plane.PointProjectPlane(point);
                    //        if (projectPoints.Where(a => a.AreEqual(newPoint, 0.05)).Count() == 0)
                    //        {
                    //            projectPoints.Add(newPoint);
                    //        }
                    //    }
                    //    if (!projectPoints.IsNullOrEmpty())
                    //    {
                    //        List<Line> faceLines = projectPoints.ContinuousPointsToLines();
                    //        string comd1212wqqwq = faceLines.ToCADCommandFile();
                    //        foreach (Line newLine in faceLines)
                    //        {
                    //            if (OptimizeLines(newLine, ref lines))
                    //            {
                    //                continue;
                    //            }
                    //            lines.Add(newLine);
                    //        }
                    //    }
                    //}
                    //List<XYZ> allPoints = lines.ToPoints();
                    //Transform planeTrans = Transform.CreateReflection(plane);
                    //Transform zTrans = Transform.CreateTranslation(new XYZ(0, 0, 1));
                    //Transform transformA = zTrans.Multiply(planeTrans);

                    //List<XYZ> allPointsTmp = new List<XYZ>();
                    //foreach (XYZ point in allPoints)
                    //{
                    //    allPointsTmp.Add(transformA.OfPoint(point));
                    //}

                    //List<XYZ> allPoints1 = GetConvexHullByAndrews(allPoints.ToArray(), viewVector).ToList();

                    //List<Line> allLines = allPoints1.ContinuousPointsToLines();
                    //string comd121222 = allLines.ToCADCommandFile();

                    #endregion


                    List<Solid> solidsTmp = new List<Solid>();
                    foreach (Face face in faces)
                    {
                        if (face is CylindricalFace cylindricalFace)
                        {
                            CurveLoop curveLoop = face.GetEdgesAsCurveLoops().FirstOrDefault();
                            if (curveLoop != null)
                            {
                                bool judge = false;
                                double length = 0;
                                foreach (Curve curve in curveLoop)
                                {
                                    if (curve is Arc arc)
                                    {
                                        List<Line> newLines = plane.PointProjectPlane(arc);
                                        if (newLines.Last().IsIntersect(newLines.First()))
                                        {
                                            try
                                            {
                                                Solid solidTmp = GeometryCreationUtilities.CreateExtrusionGeometry(
                                                    new List<CurveLoop>() { CurveLoop.Create(new List<Curve>(newLines)) },
                                                    view.ViewDirection, 2);
                                                solidsTmp.Add(solidTmp);
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            continue;
                                        }
                                        else
                                        {
                                            foreach (Line newLine in newLines)
                                            {
                                                double startDis = arc.StartPoint().DistanceTo(newLine.StartPoint());
                                                double endDis = arc.EndPoint().DistanceTo(newLine.EndPoint());
                                                if (minDis > startDis)
                                                {
                                                    minDis = startDis;
                                                }
                                                if (minDis > endDis)
                                                {
                                                    minDis = endDis;
                                                }
                                                if (OptimizeLines(newLine, ref lines))
                                                {
                                                    continue;
                                                }
                                                lines.Add(newLine);
                                            }
                                        }
                                    }
                                    else if (curve is Line line)
                                    {
                                        if (length == 0)
                                        {
                                            length = line.Length;
                                        }
                                        else
                                        {
                                            if (length.AreEqual(line.Length))
                                            {
                                                judge = true;
                                            }
                                        }
                                    }
                                    if (judge && lines.Count == 2)
                                    {
                                        double lengthTmp = lines.First().StartPoint().DistanceTo(lines.Last().StartPoint());
                                        if (lengthTmp.AreEqual(length))
                                        {
                                            Line line1 = Line.CreateBound(lines.First().StartPoint(), lines.Last().StartPoint());
                                            Line line2 = Line.CreateBound(lines.First().EndPoint(), lines.Last().EndPoint());
                                            lines.Add(line1);
                                            lines.Add(line2);
                                        }
                                        else
                                        {
                                            Line line1 = Line.CreateBound(lines.First().StartPoint(), lines.Last().EndPoint());
                                            Line line2 = Line.CreateBound(lines.First().EndPoint(), lines.Last().StartPoint());
                                            lines.Add(line1);
                                            lines.Add(line2);
                                        }
                                        string comd = lines.ToCADCommandFile();
                                    }
                                }
                            }
                        }
                        else
                        {
                            EdgeArray edgeArray = face.EdgeLoops.get_Item(0);
                            foreach (Edge edge in edgeArray)
                            {
                                Curve curve = edge.AsCurve();
                                if (curve is Arc arc)
                                {
                                    List<Line> newLines = plane.PointProjectPlane(arc);
                                    foreach (Line newLine in newLines)
                                    {
                                        double startDis = arc.StartPoint().DistanceTo(newLine.StartPoint());
                                        double endDis = arc.EndPoint().DistanceTo(newLine.EndPoint());
                                        if (minDis > startDis)
                                        {
                                            minDis = startDis;
                                        }
                                        if (minDis > endDis)
                                        {
                                            minDis = endDis;
                                        }
                                        if (OptimizeLines(newLine, ref lines))
                                        {
                                            continue;
                                        }
                                        lines.Add(newLine);
                                    }
                                }
                                else if (curve is Line line)
                                {
                                    Line newLine = plane.PointProjectPlane(line);
                                    if (newLine != null)
                                    {
                                        double startDis = line.StartPoint().DistanceTo(newLine.StartPoint());
                                        double endDis = line.EndPoint().DistanceTo(newLine.EndPoint());
                                        if (minDis > startDis)
                                        {
                                            minDis = startDis;
                                        }
                                        if (minDis > endDis)
                                        {
                                            minDis = endDis;
                                        }
                                        if (OptimizeLines(newLine, ref lines))
                                        {
                                            continue;
                                        }
                                        lines.Add(newLine);
                                    }
                                }
                            }
                        }
                    }

                    if (!solidsTmp.IsNullOrEmpty())
                    {
                        try
                        {
                            Solid solidTm = SolidEx.SolidByUnion(solidsTmp);
                            sectionFaces.Add(element.Id, new KeyValuePair<double, Solid>(minDis, solidTm));
                        }
                        catch (Exception)
                        {
                        }
                        continue;
                    }
                    string comd1212 = lines.ToCADCommandFile();
                    if (!lines.IsNullOrEmpty())
                    {
                        string comd = lines.ToCADCommandFile();
                        List<Line> newLines = new List<Line>();
                        List<List<Line>> newLiness = new List<List<Line>>();
                        GetCloseLoopLinesEx(null, ref lines, ref newLines, ref newLiness);
                        List<Line> newLinesTmp = newLiness.OrderByDescending(a => a.Sum(c => c.Length)).
                            Where(b => b.Count >= 4).Select(c => c).FirstOrDefault();

                        if (!newLinesTmp.IsNullOrEmpty())
                        {
                            if (newLinesTmp.Last().EndPoint().AreEqual(newLinesTmp.First().StartPoint(), 3))
                            {
                                if (!newLinesTmp.Last().EndPoint().AreEqual(newLinesTmp.First().StartPoint(), 0))
                                {
                                    newLinesTmp[0] = Line.CreateBound(newLinesTmp.Last().EndPoint(),
                                        newLinesTmp.First().EndPoint());
                                }
                            }
                            else
                            {
                                newLinesTmp.Add(Line.CreateBound(newLinesTmp.Last().EndPoint(),
                                    newLinesTmp.First().StartPoint()));
                            }
                            string comd1 = newLinesTmp.ToCADCommandFile();
                            try
                            {
                                List<Curve> curves = new List<Curve>(newLinesTmp);
                                CurveLoop curveLoop = CurveLoop.Create(curves);
                                Solid solidTmp = GeometryCreationUtilities.CreateExtrusionGeometry(
                                     new List<CurveLoop>() { curveLoop }, view.ViewDirection, 2);
                                sectionFaces.Add(element.Id, new KeyValuePair<double, Solid>(minDis, solidTmp));
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }

            return sectionFaces;
        }
        /// <summary>
        /// 计算从 a 到 b 再到 c 的叉积
        /// </summary>
        /// <returns>叉积值</returns>
        private static double Cross(XYZ a, XYZ b, XYZ c)
        {
            return (b.X - a.X) * (c.Y - a.Y) - (b.Y - a.Y) * (c.X - a.X);
        }
        /// <summary>
        /// 获取围绕所有点的凸多边形的轮廓点<br/>
        /// 复杂度：O(n log n)
        /// </summary>
        /// <param name="points">点数组</param>
        /// <returns>轮廓点数组</returns>
        public static XYZ[] GetConvexHullByAndrews(XYZ[] points, XYZ viewVector)
        {
            if (points.Length < 3)
            {
                throw new ArgumentException("凸包算法需要至少3个点");
            }

            // 按 x 坐标对点进行排序
            Array.Sort(points, (p1, p2) => p1.X.CompareTo(p2.X));

            // 创建下凸壳
            var lowerHull = new List<XYZ>();
            foreach (var point in points)
            {

                while (lowerHull.Count >= 2 &&
                       lowerHull[lowerHull.Count - 2].CrossProduct(lowerHull[lowerHull.Count - 1]).CrossProduct(point).AngleTo(viewVector).ToAngle() >= 90)
                //    while (lowerHull.Count >= 2 &&
                //Cross(lowerHull[lowerHull.Count - 2], lowerHull[lowerHull.Count - 1], point) <= 0)
                {
                    lowerHull.RemoveAt(lowerHull.Count - 1);
                }
                lowerHull.Add(point);
            }

            // 创建上凸壳
            var upperHull = new List<XYZ>();
            for (int i = points.Length - 1; i >= 0; i--)
            {
                var point = points[i];

                while (upperHull.Count >= 2 &&
                    upperHull[upperHull.Count - 2].CrossProduct(upperHull[upperHull.Count - 1]).CrossProduct(point).AngleTo(viewVector).ToAngle() <= 90)
                // while (upperHull.Count >= 2 &&
                // Cross(upperHull[upperHull.Count - 2], upperHull[upperHull.Count - 1], point) <= 0)

                {
                    upperHull.RemoveAt(upperHull.Count - 1);
                }
                upperHull.Add(point);
            }

            // 合并下凸壳和上凸壳
            if (lowerHull.Count == 1 && upperHull.Count == 1)
            {
                // 如果只有一个点，则返回原始点数组
                return points;
            }
            else
            {
                lowerHull.RemoveAt(lowerHull.Count - 1);
                upperHull.RemoveAt(upperHull.Count - 1);
                lowerHull.AddRange(upperHull);
                return lowerHull.ToArray();
            }
        }



        public Dictionary<ElementId, KeyValuePair<double, Solid>> GetViewSectionLines(View view, Document doc, Options option, List<Element> sourceElements)
        {
            if (view.ViewType != ViewType.Section) return null;

            FilteredElementCollector elementCollector = new FilteredElementCollector(doc, view.Id).OfCategory(BuiltInCategory.OST_Viewers);
            if (elementCollector != null)
            {
                IList<Element> elementLists = elementCollector.ToElements();
                if (!elementLists.IsNullOrEmpty())
                {
                    Element sectionElement = elementLists.Where(a => a.Name == view.Name).FirstOrDefault();
                    if (sectionElement == null) return null;
                    ViewCropRegionShapeManager viewCropRegionShape = view.GetCropRegionShapeManager();
                    List<CurveLoop> curveLoops = viewCropRegionShape.GetCropShape()?.ToList();
                    if (!curveLoops.IsNullOrEmpty())
                    {
                        if (curveLoops.Count == 1)
                        {
                            List<Line> cropShapeLines = new List<Line>();
                            foreach (Curve curve in curveLoops.First())
                            {
                                if (curve is Line line)
                                {
                                    cropShapeLines.Add(line);
                                }
                            }
                            double offsetFar = sectionElement.get_Parameter(BuiltInParameter.VIEWER_BOUND_OFFSET_FAR).AsDouble();
                            Solid solid = GeometryCreationUtilities.CreateExtrusionGeometry(curveLoops,
                                    view.ViewDirection.Copy().Negate(), offsetFar);
                            if (solid != null)
                            {
                                Dictionary<ElementId, KeyValuePair<double, Solid>> sectionFaces =
                                    new Dictionary<ElementId, KeyValuePair<double, Solid>>();
                                Plane plane = Plane.CreateByOriginAndBasis(view.Origin, view.RightDirection, view.UpDirection);
                                XYZ viewVector = view.ViewDirection.Normalize();

                                List<ElementId> elementIds = new List<ElementId>();
                                foreach (Element element in sourceElements)
                                {
                                    IList<Solid> solids = element.GetSolids(option);
                                    if (solids.Count == 1)
                                    {
                                        //获取实体的交集
                                        Solid solidBool = BooleanOperationsUtils.ExecuteBooleanOperation(
                                            solids.First(), solid, BooleanOperationsType.Intersect);
                                        if (solidBool != null && solidBool.Volume > 0)
                                        {
                                            double minDis = double.MaxValue;
                                            List<Line> lines = new List<Line>();
                                            List<Face> faces = solidBool.GetFaces();
                                            foreach (Face face in faces)
                                            {
                                                EdgeArray edgeArray = face.EdgeLoops.get_Item(0);
                                                foreach (Edge edge in edgeArray)
                                                {
                                                    Curve curve = edge.AsCurve();
                                                    if (curve is Line line)
                                                    {
                                                        Line newLine = plane.PointProjectPlane(line);
                                                        if (newLine != null)
                                                        {
                                                            GetInBoxLine(ref newLine, cropShapeLines, view.ViewDirection, true);
                                                            if (newLine.IsLineInBox(cropShapeLines, view.ViewDirection))
                                                            {
                                                                double startDis = line.StartPoint().DistanceTo(newLine.StartPoint());
                                                                double endDis = line.EndPoint().DistanceTo(newLine.EndPoint());
                                                                if (minDis > startDis)
                                                                {
                                                                    minDis = startDis;
                                                                }
                                                                if (minDis > endDis)
                                                                {
                                                                    minDis = endDis;
                                                                }
                                                                if (OptimizeLines(newLine, ref lines))
                                                                {
                                                                    continue;
                                                                }
                                                                lines.Add(newLine);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (!lines.IsNullOrEmpty())
                                            {
                                                List<Line> newLines = new List<Line>();
                                                List<List<Line>> newLiness = new List<List<Line>>();
                                                GetCloseLoopLinesEx(null, ref lines, ref newLines, ref newLiness);
                                                List<Line> newLinesTmp = newLiness.OrderByDescending(a => a.Sum(c => c.Length)).
                                                    Where(b => b.Count >= 4).Select(c => c).FirstOrDefault();

                                                if (!newLinesTmp.IsNullOrEmpty())
                                                {
                                                    if (newLinesTmp.Last().EndPoint().AreEqual(newLinesTmp.First().StartPoint()))
                                                    {
                                                        if (!newLinesTmp.Last().EndPoint().AreEqual(newLinesTmp.First().StartPoint(), 0))
                                                        {
                                                            newLinesTmp[0] = Line.CreateBound(newLinesTmp.Last().EndPoint(),
                                                                newLinesTmp.First().EndPoint());
                                                        }
                                                    }
                                                    else
                                                    {
                                                        newLinesTmp.Add(Line.CreateBound(newLinesTmp.Last().EndPoint(),
                                                            newLinesTmp.First().StartPoint()));
                                                    }
                                                    try
                                                    {
                                                        List<Curve> curves = new List<Curve>(newLinesTmp);
                                                        CurveLoop curveLoop = CurveLoop.Create(curves);
                                                        Solid solidTmp = GeometryCreationUtilities.CreateExtrusionGeometry(
                                                             new List<CurveLoop>() { curveLoop }, view.ViewDirection, 2);
                                                        sectionFaces.Add(element.Id, new KeyValuePair<double, Solid>(minDis, solidTmp));
                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                return sectionFaces;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public static void GetCloseLoopLinesEx(XYZ checkPoint,
           ref List<Line> lines, ref List<Line> newLines, ref List<List<Line>> newLiness)
        {
            if (checkPoint == null)
            {
                if (!lines.IsNullOrEmpty())
                {
                    Line lineFist = lines.OrderByDescending(a => a.Length).First();
                    newLines.Add(lineFist);
                    lines.Remove(lineFist);
                    checkPoint = lineFist.EndPoint();
                    GetCloseLoopLinesEx(checkPoint, ref lines, ref newLines, ref newLiness);
                }
            }
            else
            {
                List<Line> linesStart = lines.Where(a => a.StartPoint().AreEqual(checkPoint, 0.05)).ToList();
                if (linesStart.IsNullOrEmpty())
                {
                    List<Line> linesEnd = lines.Where(a => a.EndPoint().AreEqual(checkPoint, 0.05)).ToList();
                    if (linesEnd.IsNullOrEmpty())
                    {
                        newLiness.Add(new List<Line>(newLines));
                        newLines.Clear();
                        GetCloseLoopLinesEx(null, ref lines, ref newLines, ref newLiness);
                    }
                    else
                    {
                        if (linesEnd.Count == 1)
                        {
                            Line nextLine = Line.CreateBound(checkPoint, linesEnd.First().StartPoint());
                            lines.Remove(linesEnd.First());
                            newLines.Add(nextLine);
                            checkPoint = nextLine.EndPoint();
                            if (!checkPoint.AreEqual(newLines.First().StartPoint(), 0.05))
                            {
                                GetCloseLoopLinesEx(checkPoint, ref lines, ref newLines, ref newLiness);
                            }
                            else
                            {
                                newLiness.Add(new List<Line>(newLines));
                                newLines.Clear();
                                GetCloseLoopLinesEx(null, ref lines, ref newLines, ref newLiness);
                            }
                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    if (linesStart.Count == 1)
                    {
                        Line nextLine = Line.CreateBound(checkPoint, linesStart.First().EndPoint());
                        newLines.Add(nextLine);
                        lines.Remove(linesStart.First());
                        checkPoint = nextLine.EndPoint();
                        if (!checkPoint.AreEqual(newLines.First().StartPoint(), 0.05))
                        {
                            GetCloseLoopLinesEx(checkPoint, ref lines, ref newLines, ref newLiness);
                        }
                        else
                        {
                            newLiness.Add(new List<Line>(newLines));
                            newLines.Clear();
                            GetCloseLoopLinesEx(null, ref lines, ref newLines, ref newLiness);
                        }
                    }
                    else
                    {

                    }
                }
            }
        }

        public bool OptimizeLines(Line newLine, ref List<Line> lines)
        {
            List<Line> parallelLines = lines.Where(a => a.IsParallel(newLine, 0.01)).ToList();
            if (!parallelLines.IsNullOrEmpty())
            {
                List<Line> intersectLines = parallelLines.Where(a => a.IsIntersect(newLine, 0.01)).ToList();
                if (!intersectLines.IsNullOrEmpty())
                {
                    List<XYZ> points = new List<XYZ>() { newLine.StartPoint(), newLine.EndPoint() };
                    foreach (Line intersectLine in intersectLines)
                    {
                        points.Add(intersectLine.StartPoint());
                        points.Add(intersectLine.EndPoint());
                        lines.Remove(intersectLine);
                    }
                    var sortPoints = points.PointSort(newLine.Direction.Normalize());
                    lines.Add(Line.CreateBound(sortPoints.First(), sortPoints.Last()));
                    return true;
                }
            }
            return false;
        }

        public void GetInBoxLine(ref Line lineMain, List<Line> lines, XYZ normalVector, bool isClockwise)
        {
            foreach (Line line in lines)
            {
                XYZ intersectPoint = lineMain.GetIntersectPoint(line);
                if (intersectPoint != null)
                {
                    List<Line> breakLines = LineBreak(lineMain, intersectPoint);
                    foreach (Line breakLine in breakLines)
                    {
                        int sideState = line.PointOnLineSide(breakLine.GetMiddlePoint(), normalVector);
                        bool isInBoxState = isClockwise ? sideState == 1 : sideState == -1;
                        if (isInBoxState)
                        {
                            lineMain = breakLine;
                            break;
                        }
                    }
                }
            }
        }

        public List<Line> LineBreak(Line line, XYZ insertPoint)
        {
            List<XYZ> insertPoints = new List<XYZ>() { insertPoint };
            insertPoints.Add(line.StartPoint());
            insertPoints.Add(line.EndPoint());
            insertPoints.PointSort(line.Direction.Normalize());
            List<Line> lines = new List<Line>();
            for (int i = 0; i < insertPoints.Count - 1; i++)
            {
                if (insertPoints[i].AreEqual(insertPoints[i + 1])) continue;
                Line newLine = Line.CreateBound(insertPoints[i], insertPoints[i + 1]);
                lines.Add(newLine);
            }
            return lines;
        }

    }
}
