﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class CurveEx
    {
        /// <summary>
        /// 获取线段起始点
        /// </summary>
        /// <param name="curve"></param>
        /// <returns></returns>
        public static XYZ StartPoint(this Curve curve)
        {
            return curve.GetEndPoint(0);
        }

        /// <summary>
        /// 获取线段结束点
        /// </summary>
        /// <param name="curve"></param>
        /// <returns></returns>
        public static XYZ EndPoint(this Curve curve)
        {
            return curve.GetEndPoint(1);
        }
    }
}
