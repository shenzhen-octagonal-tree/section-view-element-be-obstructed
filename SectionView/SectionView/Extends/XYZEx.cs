﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class XYZEx
    {
        public static XYZ GetMiddlePt(this XYZ pt1, XYZ pt2)
        {
            return new XYZ((pt1.X + pt2.X) / 2, (pt1.Y + pt2.Y) / 2, (pt1.Z + pt2.Z) / 2);
        }
        public static XYZ Copy(this XYZ point)
        {
            return new XYZ(point.X, point.Y, point.Z);
        }

        public static bool AreEqual(this XYZ pt1, XYZ pt2, double tolerance = 0.1)
        {
            if (pt1.DistanceTo(pt2) < tolerance)
            {
                return true;
            }
            else
            {

                return false;
            }
        }
        public static bool IsPointOnLine(this XYZ point, Line line, double tolerance = 0.1)
        {
            double numLength = line.StartPoint().DistanceTo(line.EndPoint());
            double distanceA = line.StartPoint().DistanceTo(point);
            double distanceB = line.EndPoint().DistanceTo(point);
            if (Math.Abs(numLength - distanceA - distanceB) < tolerance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
