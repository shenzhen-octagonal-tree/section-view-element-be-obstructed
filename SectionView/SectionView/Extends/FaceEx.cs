﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class FaceEx
    {  
        /// <summary>
       /// 获取面上中心点(世界坐标)
       /// </summary>
       /// <param name="face"></param>
       /// <returns></returns>
        public static XYZ GetUVCenterPoint(this Face face)
        {
            var boundingBox = face.GetBoundingBox();
            UV centerUV = (boundingBox.Max + boundingBox.Min).Divide(2);
            XYZ faceNormal = face.Evaluate(centerUV);
            return faceNormal;
        }

        /// <summary>
        /// 获取面上中心点向量(法线方向)
        /// </summary>
        /// <param name="face"></param>
        /// <returns></returns>
        public static XYZ GetUVCenterPointNormal(this Face face)
        {
            var boundingBox = face.GetBoundingBox();
            UV centerUV = (boundingBox.Max + boundingBox.Min).Divide(2);
            XYZ faceNormal = face.ComputeNormal(centerUV);
            return faceNormal;
        }
    }
}
