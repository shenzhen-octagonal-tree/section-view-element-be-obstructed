﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class PlaneEx
    {
        public static XYZ PointProjectPlane(this Plane plane, XYZ xyz)
        {
            Transform tf = Transform.Identity;
            tf.BasisX = plane.XVec;
            tf.BasisY = plane.YVec;
            tf.BasisZ = plane.Normal;
            tf.Origin = plane.Origin;
            XYZ p = tf.Inverse.OfPoint(xyz);
            p = new XYZ(p.X, p.Y, 0);
            return tf.OfPoint(p);
        }


        public static Line PointProjectPlane(this Plane plane, Line line)
        {
            XYZ newStartXYZ = plane.PointProjectPlane(line.StartPoint().Copy());
            XYZ newEndXYZ = plane.PointProjectPlane(line.EndPoint().Copy());
            if (newStartXYZ.DistanceTo(newEndXYZ) > 0.5)
            {
                return Line.CreateBound(newStartXYZ, newEndXYZ);
            }
            return null;
        }

        public static List<Line> PointProjectPlane(this Plane plane, Arc arc)
        {
            List<XYZ> points = arc.Tessellate().ToList();
            string comd = points.ContinuousPointsToLines().ToCADCommandFile();
            List<XYZ> newPoints = new List<XYZ>();
            foreach (XYZ point in points)
            {
                XYZ newPoint = plane.PointProjectPlane(point);
                newPoints.Add(newPoint);
            }
            return newPoints.ContinuousPointsToLines();
            //XYZ vectorA = (newPoints[1] - newPoints[1]).Normalize();
            //XYZ vectorB = (newPoints[2] - newPoints[1]).Normalize();
            //if (vectorA.AreEqual(vectorB, 0.05))
            //{

            //}
        }
    }
}
