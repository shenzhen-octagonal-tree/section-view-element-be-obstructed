﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class ElementEx
    {
        public static void CancelJoinAndCut(this Element element, Document doc)
        {
            try
            {
                ICollection<ElementId> joinElements = JoinGeometryUtils.GetJoinedElements(doc, element);
                if (!joinElements.IsNullOrEmpty())
                {
                    foreach (ElementId id in joinElements)
                    {
                        Element joinElement = doc.GetElement(id);
                        JoinGeometryUtils.UnjoinGeometry(doc, joinElement, element);
                    }
                }
                ICollection<ElementId> cutElements = SolidSolidCutUtils.GetCuttingSolids(element);
                if (!cutElements.IsNullOrEmpty())
                {
                    foreach (ElementId id in cutElements)
                    {
                        Element cutElement = doc.GetElement(id);
                        SolidSolidCutUtils.RemoveCutBetweenSolids(doc, cutElement, element);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public static IList<Solid> GetSolids(this Element element, Options geomOption)
        {
            List<Solid> solids = new List<Solid>();
            GeometryElement geomElem = element.get_Geometry(geomOption);
            if (geomElem != null)
            {
                double volume = 0;
                GetAllSolids(geomElem, ref solids, ref volume);
            }
            return solids;
        }

        private static void GetAllSolids(GeometryElement geomElem, ref List<Solid> solids, ref double volume)
        {
            foreach (GeometryObject geomObj in geomElem)
            {
                if (geomObj is Solid solid)
                {
                    if (solid != null && solid.Faces.Size > 0 && solid.Volume > 0)
                    {
                        volume += solid.Volume;
                        solids.Add(solid);
                        continue;
                    }
                }
                else if (geomObj is GeometryInstance geomInst)
                {
                    GeometryElement instGeomElem = geomInst.GetInstanceGeometry();
                    GetAllSolids(instGeomElem, ref solids, ref volume);
                    continue;
                }
                else if (geomObj is GeometryElement geometryElement)
                {
                    GetAllSolids(geometryElement, ref solids, ref volume);
                    continue;
                }
            }
        }
    }
}
