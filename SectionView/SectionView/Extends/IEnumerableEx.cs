﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class IEnumerableEx
    {
        /// <summary>
        /// 线集合对象转成CAD命令脚本文件
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        internal static string ToCADCommandFile(this IEnumerable<Line> lines)
        {
            string optimizeLineCADCommand = string.Empty;
            if (!lines.IsNullOrEmpty())
            {
                foreach (Line line in lines)
                {
                    optimizeLineCADCommand += line.ToCADCommand();
                }
            }
            return optimizeLineCADCommand;
        }
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            if (list != null && list.Count() > 0)
            {
                return false;
            }
            return true;
        }

        public static List<XYZ> PointSort(this IEnumerable<XYZ> points, XYZ vector)
        {
            XYZ first;
            XYZ second;
            List<XYZ> newPoints = new List<XYZ>();
            newPoints = points.Select(item => item).ToList();
            for (int i = 0; i < newPoints.Count - 1; i++)
            {
                for (int j = 0; j < newPoints.Count - i - 1; j++)
                {
                    first = newPoints[j];
                    second = newPoints[j + 1];
                    XYZ tmvector = (second - first).Normalize();
                    if (!tmvector.AreEqual(vector))
                    {
                        newPoints[j + 1] = first;
                        newPoints[j] = second;
                    }
                }
            }
            return newPoints;
        }

        /// <summary>
        /// 连续点构造多条线段
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>

        public static List<Line> ContinuousPointsToLines(this List<XYZ> points)
        {
            List<Line> lines = new List<Line>();
            for (int i = 0; i <= points.Count - 1; i++)
            {
                if (i == points.Count - 1)
                {
                    if (points[points.Count - 1].AreEqual(points[0])) continue;
                    Line line = Line.CreateBound(points[points.Count - 1], points[0]);
                    lines.Add(line);
                }
                else
                {
                    if (points[i].AreEqual(points[i + 1])) continue;
                    Line line = Line.CreateBound(points[i], points[i + 1]);
                    lines.Add(line);
                }
            }
            return lines;
        }

        public static List<XYZ> ToPoints(this IEnumerable<Line> lines)
        {
            List<XYZ> allPoints = new List<XYZ>();
            foreach (Line line in lines)
            {
                var startPt = line.StartPoint();
                if (allPoints.Where(a => a.AreEqual(startPt)).Count() == 0)
                {
                    allPoints.Add(startPt);
                }

                var endPt = line.EndPoint();

                if (allPoints.Where(a => a.AreEqual(endPt)).Count() == 0)
                {
                    allPoints.Add(endPt);
                }
            }
            return allPoints;
        }
    }
}
