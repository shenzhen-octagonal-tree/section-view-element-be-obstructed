﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class LineEx
    {
        public static string ToCADCommand(this Line line)
        {
            if (line == null) return string.Empty;
            return string.Format("(command \"line\" '({0} {1} {2}) '({3} {4} {5}) \"\")\n",
                line.StartPoint().X, line.StartPoint().Y, line.StartPoint().Z,
                line.EndPoint().X, line.EndPoint().Y, line.EndPoint().Z);
        }
        public static bool IsParallel(this Line lineA, Line lineB, double tolerance = 0.1)
        {
            var vectorA = lineA.Direction.Normalize();
            var vectorB = lineB.Direction.Normalize();
            return (vectorA.Normalize().CrossProduct(vectorB.Normalize())).GetLength() < tolerance;
        }

        public static bool IsIntersect(this Line lineMain, Line lineNext, double tolerance = 0.1)
        {
            if (lineMain.StartPoint().IsPointOnLine(lineNext, tolerance))
            {
                return true;
            }
            if (lineMain.EndPoint().IsPointOnLine(lineNext, tolerance))
            {
                return true;
            }
            if (lineNext.StartPoint().IsPointOnLine(lineMain, tolerance))
            {
                return true;
            }
            if (lineNext.EndPoint().IsPointOnLine(lineMain, tolerance))
            {
                return true;
            }
            return false;
        }
        public static XYZ GetMiddlePoint(this Curve curve)
        {
            return curve.StartPoint().GetMiddlePt(curve.EndPoint());
        }

        public static XYZ GetIntersectPoint(this Line lineMain, Line lineNext)
        {
            if (lineMain.Intersect(lineNext, out IntersectionResultArray resultArray).Equals(SetComparisonResult.Overlap))
            {
                if (resultArray.Size == 1)
                {
                    IntersectionResult intersectionResult = resultArray.get_Item(0);
                    return intersectionResult?.XYZPoint;
                }
            }
            return null;
        }

        public static bool IsLineInBox(this Line lineMain, List<Line> lines, XYZ normalVector)
        {
            int? state = null;
            List<XYZ> points = new List<XYZ>() { lineMain.StartPoint(), lineMain.EndPoint() };
            foreach (XYZ point in points)
            {
                foreach (Line line in lines)
                {
                    int stateTmp = line.PointOnLineSide(point, normalVector);
                    if (stateTmp != 0)
                    {
                        if (state == null)
                        {
                            state = stateTmp;
                        }
                        else if (stateTmp != state)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public static int PointOnLineSide(this Line lineMain, XYZ point, XYZ normalVector)
        {
            if (point.IsPointOnLine(lineMain))
            {
                return 0;
            }
            XYZ vectorA = (lineMain.EndPoint() - lineMain.StartPoint()).Normalize();
            XYZ vectorB = (point - lineMain.StartPoint()).Normalize();
            XYZ vector = vectorB.CrossProduct(vectorA);
            double angle = normalVector.Normalize().AngleTo(vector).ToAngle();
            if (angle <= 90)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
