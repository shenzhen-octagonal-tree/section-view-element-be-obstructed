﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    public static class DoubleEx
    {
        /// <summary>
        /// 判断两个值是否相等
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool AreEqual(this double value1, double value2, double tolerance = 0.1)
        {
            if (Math.Abs(value1 - value2) < tolerance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static double ToAngle(this double radian)
        {
            return radian * 180 / Math.PI;
        }
    }
}
