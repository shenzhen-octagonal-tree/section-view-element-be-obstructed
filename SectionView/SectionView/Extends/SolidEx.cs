﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SectionView.Extends
{
    static class SolidEx
    {
        public static List<Face> GetFaces(this Solid solid)
        {
            List<Face> Faces = null; ;
            if (solid != null)
            {
                FaceArray faces = solid.Faces;
                if (faces != null && faces.Size > 0)
                {
                    Faces = new List<Face>();
                    foreach (Face face in faces)
                    {
                        Faces.Add(face);
                    }
                }
            }
            return Faces;
        }
        /// <summary>
        /// 将多个solid合并成一个solid
        /// </summary>
        /// <param name="solids"></param>
        /// <returns></returns>
        public static Solid SolidByUnion(List<Solid> solids)
        {
            Solid result;
            if (solids.Count > 2)
            {
                Solid solid1 = solids[0];
                solids.RemoveAt(0);
                Solid solid2 = SolidByUnion(solids);
                result = BooleanOperationsUtils.ExecuteBooleanOperation(solid1, solid2, BooleanOperationsType.Union);
                return result;
            }
            else
            {
                Solid solid1 = solids[0];
                Solid solid2 = solids[1];
                result = BooleanOperationsUtils.ExecuteBooleanOperation(solid1, solid2, BooleanOperationsType.Union);
                return result;
            }
        }
    }
}
